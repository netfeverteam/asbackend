/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.utils;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 28/08/18
 */
public class TestFileUtils {
    @DataProvider
    public Object[][] dpTailNLineBefore() {
        return new Object[][] {
                {
                        String.format("/%s/last-100-lines.log", TestFileUtils.class.getName().replace(".", "/")),
                        "/logs/catalina.2018-05-25.log"
                }
        };
    }

    @Test(dataProvider = "dpTailNLineBefore")
    public void testTailNLineBefore(String logFileResourcePath, String expectedResourcePath) throws Exception {
        List<String> expectedLines = Files.readAllLines(Paths.get(TestFileUtils.class.getResource(logFileResourcePath).toURI()));

        List<String> lines = new ArrayList<>();

        FileUtils.tailNLineBefore(
                TestFileUtils.class.getResource(expectedResourcePath).getPath(),
                100,
                lines::add);

        assert lines.containsAll(expectedLines) :
                String.format("expected:\n%s\n\nbut found:\n%s", expectedLines, lines);
    }
}
