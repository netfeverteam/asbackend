<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <style>
        html,
        body {
            height: 100%;
            margin: 0
        }

        .log {
            font-family: monospace;
            font-size: 12px;
            font-style: normal;
            font-variant: normal;
            font-weight: 400;
            white-space: pre;

        }

        .box {
            display: flex;
            flex-flow: column;
            height: 100%;
        }

        .box .row {
            border: 1px dotted grey;
        }

        .box .row.header {
            flex: 0 1 auto;
        }

        .box .row.content {
            flex: 1 1 auto;
        }

        .box .row.footer {
            flex: 0 1 40px;
        }
    </style>

    <script>
        function addLine(line) {
            let tokens = document.getElementById("grep").value.split(",");

            tokens.forEach(function (token) {
                token = token.trim();

                if (token.length > 0) {
                    line = line.replace(token, "<font color='red'><b>" + token + "</b></font>");
                }
            });

            document.getElementById("result").innerHTML += line + "<br>";
        }

        function seeLog() {
            if(typeof(EventSource) !== "undefined") {
                let id = document.getElementById("logid").value;
                let linesBack = parseInt(document.getElementById("linesback").value);

                if (isNaN(linesBack)) {
                    linesBack = 1000;
                    document.getElementById("linesback").value = "1000";
                }

                <%
                    String url = request.getRequestURL().toString();
                    int pos = url.indexOf(request.getContextPath());
                    url = url.substring(0, pos + request.getContextPath().length());

                    if ("1".equals(request.getHeader("upgrade-insecure-requests"))) {
                        url = url.replace("http:", "https:");
                    }
                %>
                let source = new EventSource("<%=url%>/services/v1/file/tail/" + id + "?linesBack=" + linesBack);

                document.getElementById("result").innerHTML = "";

                source.addEventListener("keepalive", function(event) {
                    //document.getElementById("result").innerHTML += event.data + "<br>";
                });

                source.addEventListener("logline", function(event) {
                    addLine(event.data);
                    scrollDown();
                });

                source.addEventListener("backlog", function(event) {
                    document.getElementById("result").innerHTML = "";

                    let lines = JSON.parse(event.data);
                    lines.forEach(function(line) {
                        addLine(line);
                    });

                    scrollDown();
                });
            } else {
                document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
            }
        }

        function scrollDown() {
            let objDiv = document.getElementById("result");
            objDiv.scrollTop = objDiv.scrollHeight;
            //window.scrollTo(0,document.body.scrollHeight);
        }
    </script>
</head>
<body>

<div class="box">
    <div class="row header">
        <h3>
            Tail Server Log <input type="text" name="logid" id="logid" />&nbsp;
            lines back <input type="number" name="linesback" id="linesback" value="1000"/> &nbsp;
            grep (comma separated) <input type="text" name="grep" id="grep" value=""/> &nbsp;
            <button onclick="seeLog()">Fetch</button>
        </h3>
        <hr/>
    </div>

    <div class="row content log" id="result" style="overflow:auto">

    </div>

    <div class="row footer">
        <hr/>
        Sources on <a href="https://bitbucket.org/account/user/netfeverteam/projects/AL">bitbucket</a>.
        (c) 2018 <a href="mailto:yoram.halberstam@gmail.com">Yoram Halberstam</div>
    </div>
</div>

</body>
</html>
