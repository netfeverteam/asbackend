/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 28/08/18
 */
public class Config {
    private static final Logger LOG = LogManager.getLogger(Config.class);

    private static final Timer TIMER = new Timer();

    private static Properties logFiles = null;
    private static final Object LOG_FILES_LOCK = new Object();

    public static File getAlloServerConfigDir() {
        return new File(System.getProperty("allo.server.config.dir"));
    }
    public static Timer getTimer() {
        return TIMER;
    }

    public static String getPathForId(final String id) {
        if (logFiles == null) {
            try {
                final File fLogFiles = new File(getAlloServerConfigDir(), "log-files.properties");

                if (fLogFiles.isFile()) {
                    synchronized (LOG_FILES_LOCK) {
                        Properties props = new Properties();

                        try (final InputStream in = new FileInputStream(fLogFiles)) {
                            props.load(in);
                        }

                        logFiles = props;
                    }
                }
            } catch (Throwable t) {
                LOG.error(t.getMessage(), t);
            }
        }

        return logFiles == null ? null : logFiles.getProperty(id);
    }
}
