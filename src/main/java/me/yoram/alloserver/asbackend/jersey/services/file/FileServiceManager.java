/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.jersey.services.file;

import me.yoram.alloserver.asbackend.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.TimerTask;

/**
 * @author theyoz
 * @since 28/08/18
 */
class FileServiceManager {
    private static final Logger LOG = LogManager.getLogger(FileServiceManager.class);

    private final SseBroadcaster sseBroadcaster = new SseBroadcaster();

    FileServiceManager(final String path) {
        Config.getTimer().schedule(new TimerTask() {
            private final File f = new File(path);
            private long lastPosition = f.length();

            @Override
            public void run() {
                try {
                    final long len = f.length();

                    if (len != lastPosition) {
                        // Reading and writing file
                        final RandomAccessFile raf = new RandomAccessFile(f, "r");
                        raf.seek(lastPosition);
                        String line;

                        while ((line = raf.readLine()) != null) {
                            getSseBroadcaster().broadcast(
                                    new OutboundEvent.Builder()
                                            .name("logline")
                                            .comment("log line")
                                            .mediaType(MediaType.TEXT_PLAIN_TYPE)
                                            .data(line)
                                            .build());
                        }

                        lastPosition = len;
                    }
                } catch (Throwable t) {
                    LOG.error(t.getMessage(), t);
                }
            }
        }, 0, 500);
    }

    SseBroadcaster getSseBroadcaster() {
        return sseBroadcaster;
    }

    void keepAlive() {
        sseBroadcaster.broadcast(
                new OutboundEvent.Builder()
                        .name("keepalive")
                        .comment("")
                        .mediaType(MediaType.TEXT_PLAIN_TYPE)
                        .data(new Date().toString())
                        .build());
    }
}
