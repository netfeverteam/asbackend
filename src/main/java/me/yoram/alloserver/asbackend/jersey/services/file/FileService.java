/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.jersey.services.file;

import com.google.gson.Gson;
import me.yoram.alloserver.asbackend.config.Config;
import me.yoram.alloserver.asbackend.utils.FileUtils;
import me.yoram.alloserver.asbackend.utils.KeepAliveTimerTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.glassfish.jersey.media.sse.SseFeature;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author theyoz
 * @since 28/08/18
 */
@Path("/v1/file")
public class FileService {
    private static final Logger LOG = LogManager.getLogger(FileService.class);

    private static final Map<String, FileServiceManager> MAP = new ConcurrentHashMap<>();

    static {
        try {
            KeepAliveTimerTask.register(FileService.class);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void keepAlive() {
        MAP.values().parallelStream().forEach(FileServiceManager::keepAlive);
    }

    @GET
    @Produces({SseFeature.SERVER_SENT_EVENTS, MediaType.APPLICATION_JSON})
    @Path("/tail/{ID}")
    public EventOutput tailFile(
            @PathParam("ID") final String fileId,
            @QueryParam("linesBack") @DefaultValue("100") final int linesBack) {
        LOG.debug(String.format("tailFile(\"%s\", %d)", fileId, linesBack));

        final String path = Config.getPathForId(fileId);

        final EventOutput eventOutput = new EventOutput();

        final FileServiceManager mgr = MAP.computeIfAbsent(fileId, s -> new FileServiceManager(path));

        try {
            final List<String> lines = new ArrayList<>();

            if (linesBack < 1) {
                FileUtils.readFile(path, lines::add);
            } else {
                FileUtils.tailNLineBefore(path, linesBack, lines::add);
            }

            final SseBroadcaster sseBroadcaster = new SseBroadcaster();
            sseBroadcaster.add(eventOutput);
            sseBroadcaster.broadcast(
                    new OutboundEvent.Builder()
                            .name("backlog")
                            .comment("backlog")
                            .mediaType(MediaType.APPLICATION_JSON_TYPE)
                            .data(new Gson().toJson(lines))
                            .build());
        } catch (Throwable t) {
            LOG.error(t.getMessage(), t);
        }

        mgr.getSseBroadcaster().add(eventOutput);

        return eventOutput;
    }
}
