/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.jersey.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 02/09/18
 */
@Provider
@PreMatching
public class TransactionIdFilter implements ContainerRequestFilter {
    private static final Logger LOG = LogManager.getLogger(TransactionIdFilter.class);

    @Override
    public void filter(ContainerRequestContext reqContext) {
        ThreadContext.put("transactionId", UUID.randomUUID().toString());
        LOG.info("Starting call to server");
    }
}