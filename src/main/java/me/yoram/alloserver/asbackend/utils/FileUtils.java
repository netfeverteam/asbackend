/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.utils;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 28/08/18
 */
public class FileUtils {
    public interface ILogLineEvent {
        void onLine(String line);
    }

    public static void readFile(
            final String filepath, final ILogLineEvent event) throws IOException  {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filepath)))) {
            String line;

            while ((line = reader.readLine()) != null) {
                event.onLine(line);
            }
        }
    }

    public static void tailNLineBefore(
            final String filepath, final int upToWhatLine, final ILogLineEvent event) throws IOException {
        try (final RandomAccessFile raf = new RandomAccessFile(filepath, "r")) {
            final Deque<Long> linePos = new ArrayDeque<>(upToWhatLine);
            final byte[] buf = new byte[4096];
            int nread;
            long currentPos = -1L;

            while ((nread = raf.read(buf)) != -1) {
                for (int i = 0; i < nread; i++) {
                    currentPos++;

                    if (buf[i] == 10) {
                        if (linePos.size() == upToWhatLine) {
                            linePos.removeFirst();
                        }

                        linePos.add(currentPos);
                    }
                }
            }

            raf.seek(linePos.isEmpty() ? 0 : linePos.getFirst());
            String line;

            while ((line = raf.readLine()) != null) {
                event.onLine(line);
            }
        }
    }
}
