/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.alloserver.asbackend.utils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 28/08/18
 */
public class KeepAliveTimerTask extends TimerTask {
    private static class KeepAliveWrapper implements Comparator<KeepAliveWrapper> {
        private final Method m;

        private KeepAliveWrapper(final Class<?> clazz) throws NoSuchMethodException {
            m = clazz.getMethod("keepAlive");

            if (!Modifier.isStatic(m.getModifiers())) {
                throw new IllegalArgumentException(
                        String.format("No static method keepAlive in class %s", clazz.getName()));
            }
        }

        private void keepAlive() {
            try {
                m.invoke(null);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return String.format("KeepAliveTimerTask{%s}", m.getDeclaringClass().getName());
        }

        @Override
        public int hashCode() {
            return m.getDeclaringClass().hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof KeepAliveWrapper && m == ((KeepAliveWrapper)obj).m;
        }

        @Override
        public int compare(KeepAliveWrapper o1, KeepAliveWrapper o2) {
            if (o1 == null && o2 == null) {
                return 0;
            } else if (o1 == null) {
                return 1;
            } else if (o2 == null) {
                return -1;
            } else {
                return o1.m.toString().compareTo(o2.m.toString());
            }
        }
    }

    private static final Set<KeepAliveWrapper> SET = new CopyOnWriteArraySet<>();

    public static void register(final Class<?> clazz) throws NoSuchMethodException {
        SET.add(new KeepAliveWrapper(clazz));
    }

    @Override
    public void run() {
        SET.parallelStream().forEach(KeepAliveWrapper::keepAlive);
    }
}
