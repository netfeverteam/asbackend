package me.yoram.alloserver.asbackend;

import me.yoram.alloserver.asbackend.config.Config;
import me.yoram.alloserver.asbackend.jersey.filters.TransactionIdFilter;
import me.yoram.alloserver.asbackend.utils.KeepAliveTimerTask;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class PubApplication extends ResourceConfig {
    public PubApplication() {
        packages("me.yoram.alloserver.asbackend.jersey.services");

        register(TransactionIdFilter.class);

        // if you want J2EE roles
        register(RolesAllowedDynamicFeature.class);

        // if you want to include Multipart filter
        // register(MultiPartFeature.class);

        Config.getTimer().schedule(new KeepAliveTimerTask(), 0, 10000);
    }
}